module.exports = function(){
    
    var lettere = function (req, res){
       var vocali = req.query.testo.match(/[aeiuo]/gi);
       var consonanti = req.query.testo.match(/[qwrtypsdfghjklzxcvbnm]/gi);
       
       var vocaliCont = (!vocali) ? 0 : vocali.length;
       var consonantiCont = (!consonanti) ? 0 : consonanti.length;
       var totale = {
           consonanti : consonantiCont,
           vocali : vocaliCont
       };
       res.send(totale);
    };
    
    var parole = function(req, res){
        var str = req.query.testo;
        var j = 0;
        var i = 0;
        var temp;
        var arr = str.split(" ");
        
        for (i = arr.length -1; i>0; i-= 1){
            j= Math.floor(Math.random() * (i + 1))
            temp = arr[i];
            arr[i] = arr[j];
            arr[j]= temp;
        };
        
        res.send(arr);  
    };
    
    var cammello = function(req, res){
        var stringa = req.query.testo;
        var arr = stringa.split("");
        var result = [];
        
        arr.forEach(function(lettera, index){
            if(index%2===0){
                lettera = lettera.toUpperCase();
            } else{
                lettera = lettera.toLowerCase();
            };
            result.push(lettera);
        });
        res.send(result.join(""));
    };
    
    var frasiGenerate = function (req, res){
        var argomento = req.query.testo;
        var array = ['scienza', 'astrofisica', 'null'];
        var frasi = {
            'scienza' :'E tuttavia il risultato dell esperienza di molti anni di lavoro ottenuto facendo ricorso ai risultati della ricerca più avanzata purché non si dimentichi il principio di sovrapposizione anche se, talvolta, ciò non sia rigorosamente necessario consente di avvicinare i grandi temi della cosmologia moderna e una nuova, individuane dei meccanismi di produzione di energia non termica quasi sempre coinvolti nella dinamica, dei fenomeni astrofisici.', 
            'astrofisica' : 'Il confronto delle abbondanze chimiche negli oggetti galattici applicato all\'interpretazione einsteiniana dei fenomeni con un preciso riferimento al suo contenuto classico attraverso un \'opportuna definizione della metrica suggerisce varie possibilità di soluzione del problema cosmologico e un\'adeguata. valutazione del peso da attribuire alla presenza dei neutrini assai spesso chiamati in causa dagli sviluppi teorici più avanzati.',
            'null':'null'
            };
            
        if (array.indexOf(argomento)>-1){
            res.send(frasi[argomento]);
        }else{
            res.send('nessuna frase trovata');
        };      
    };
    
    var anagramma = function(req, res){
        var params = [];
        var random = 0;
        var arr= [];
        var check = 0;
        var temp;
        var json = JSON.parse(JSON.stringify(require('../wordList/WordList.json')));
        var result = [];
        
        //N params
        for (var i in req.query) {
            params.push(req.query[i]);
        };
        params.map( function(index){
            console.log(index);
            do{
                var arr = index.split("");
                console.log(index);
                //Random
                for (i = arr.length -1; i>0; i-= 1){
                    random = Math.floor(Math.random() * (i + 1))
                    temp = arr[i];
                    arr[i] = arr[random];
                    arr[random]= temp;
                };
                
                arr = arr.join('');
                
                //match
                json.forEach( function (value, index){
                    if (value == arr){
                        check = 1;
                        result.push(arr);
                    };
                });
            } while(check == 0);
        });
        console.log(result);
        res.status(200).send(result);
    }
    

    return {
        lettere : lettere,
        parole : parole,
        cammello : cammello,
        frasiGenerate : frasiGenerate,
        anagramma: anagramma
        
    }
}