var express = require('express');
var controllerNumeri = require('./numeri/numeri.controller.js')();
var controllerLettere = require('./lettere/lettere.controller.js')();
var router = express.Router();

router.get('/numeri/calcolatrice', controllerNumeri.calcolatrice);
router.get('/lettere/lettere', controllerLettere.lettere);
router.get('/lettere/parole', controllerLettere.parole);
router.get('/lettere/cammello', controllerLettere.cammello);
router.get('/lettere/frasiGenerate', controllerLettere.frasiGenerate);
router.get('/lettere/anagramma', controllerLettere.anagramma);

module.exports = router;